var knex = require('./routes/dbcon');
const bcrypt = require('bcryptjs');

async function fillDatabase() {

    //DROP ALL TABLES
    await knex.schema.dropTableIfExists('rating').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('event_has_users').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('favourite').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('comment').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('place').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('event').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('user').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('images').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('location').catch((err) => { console.log(err); throw err });
    await knex.schema.dropTableIfExists('sport').catch((err) => { console.log(err); throw err });

    // SPORT TABLE
    await knex.schema.createTable('sport', (table) => {
        table.increments('sport_id').primary();
        table.string('name');
    }).then(() => console.log("Sport database created"))
        .catch((err) => { console.log(err); throw err });

    // LOCATION TABLE
    await knex.schema.createTable('location', (table) => {
        table.increments('location_id').primary();
        table.string('street');
        table.string('street_no');
        table.string('city');
    }).then(() => console.log("Location database created"))
        .catch((err) => { console.log(err); throw err });

    //IMAGES TABLE
    await knex.schema.createTable('images', (table) => {
        table.increments('image_id').primary();
        table.string('image',1000);
    }).then(() => console.log("event Images database created"))
        .catch((err) => { console.log(err); throw err });
        
    // ACCOUNTTABLE TABLE
    await knex.schema.createTable('user', (table) => {
        table.increments('user_id').primary();
        table.string('name');
        table.string('surname');
        table.string('email');
        table.string('contact');
        table.string('gender');
        table.string('description',1000);
        table.string('UID');
        table.boolean('admin');
        table.integer('tk_id_image').unsigned().references('image_id').inTable('images');
        table.integer('tk_id_location').unsigned().references('location_id').inTable('location');
    }).then(() => console.log("user database created"))
        .catch((err) => { console.log(err); throw err });

    // RATING TABLE
    await knex.schema.createTable('rating', (table) => {
        table.increments('rating_id').primary();
        table.integer('grade');
        table.integer('tk_user_id').unsigned().references('user_id').inTable('user');
        table.string('tk_rater_uid',128).references('UID').inTable('user');
    }).then(() => console.log("Rating database created"))
        .catch((err) => { console.log(err); throw err });

    // EVENT TABLE
    await knex.schema.createTable('event', (table) => {
        table.increments('event_id').primary();
        table.integer('tk_id_location').unsigned().references('location_id').inTable('location');
        table.integer('tk_id_sport').unsigned().references('sport_id').inTable('sport');
        table.integer('tk_id_organizator').unsigned().references('user_id').inTable('user');
        table.integer('tk_id_image').unsigned().references('image_id').inTable('images');
        table.integer('capacity');
        table.string('title');
        table.string('description', 1000);
        table.date('date');
    }).then(() => console.log("Event database created"))
        .catch((err) => { console.log(err); throw err });

    //PLACE TO BE RENTED TABLE
    await knex.schema.createTable('place', (table) => {
        table.increments('place_id').primary();
        table.integer('tk_renter_id').unsigned().references('user_id').inTable('user');
        table.integer('tk_location_id').unsigned().references('location_id').inTable('location');
        table.integer('tk_id_image').unsigned().references('image_id').inTable('images');
        table.string('title');
        table.string('description', 1000);
        table.integer('capacity');
        table.integer('price');
        table.date('date');
    }).then(() => console.log("Place database created"))
        .catch((err) => { console.log(err); throw err });

    // COMMENT TABLE
    await knex.schema.createTable('comment', (table) => {
        table.increments('comment_id').primary();
        table.integer('tk_user_id').unsigned().references('user_id').inTable('user');
        table.integer('tk_event_id').unsigned().references('event_id').inTable('event');
        table.string('text');
        table.date('date');
    }).then(() => console.log("Comments database created"))
        .catch((err) => { console.log(err); throw err });

    //FAVOURITE TABLE
    await knex.schema.createTable('favourite', (table) => {
        table.increments('favourite_id').primary();
        table.integer('tk_user_id').unsigned().references('user_id').inTable('user');
        table.integer('tk_sport_id').unsigned().references('sport_id').inTable('sport');
    }).then(() => console.log("Favourite database created"))
        .catch((err) => { console.log(err); throw err });

    //EVENT HAS USERS TABLE
    await knex.schema.createTable('event_has_users', (table) => {
        table.increments('event_has_users_id').primary();
        table.integer('tk_user_id').unsigned().references('user_id').inTable('user');
        table.integer('tk_event_id').unsigned().references('event_id').inTable('event');
    }).then(() => console.log("Event has users database created"))
        .catch((err) => { console.log(err); throw err });

    // INITIAL VALUES FOR TABLES
    const imgs = [{
        image: "balet.jpg"
    },
    {
        image: "basketball.jpg"
    },
    {
        image: "bookclub.webp"
    },
    {
        image: "cycling.jpg"
    },
    {
        image: "gym.jpg"
    },
    {
        image: "hiking.jpg"
    },
    {
        image: "maraton.jpg"
    },
    {
        image: "yoga.jpg"
    },
    {
        image: "yogaPlace.jpg"
    },
    {
        image: "beachvolley.jpg"
    },
    {
        image: "pilates.jpg"
    },
    {
        image: "uros.webp"
    },
    {
        image: "Miki.jpg"
    },
    {
        image: "builder.jpg"
    },
    {
        image: "footballer.jpg"
    },
    {
        image: "banner.jpg"
    },
    {
        image: "blank-profile-picture-973460_1280.png"
    }
    ]
    const user = [{
        name: "Pera",
        surname: "Peric",
        email: "pera.peric@student.um.si",
        uid: "XkAXotJYmGMmJZBJFqNvPYxdEZ52",
        gender: "M",
        contact: "+38654061588",
        admin: false,
        tk_id_location: 1,
        tk_id_image: 14,
        description:'Hello! I am Peter and I hope you can join me here so we can excerise together'
    },
    {
        name: "Grega",
        surname: "Novak",
        email: "admin@gmail.com",
        uid: 1234,
        gender: "M",
        contact: "+38654061588",
        admin: true,
        tk_id_location: 1,
        tk_id_image: 15
    },
    {
        name: "Miki",
        surname: "Miric",
        email: "miki@gmail.com",
        uid: 3241,
        gender: "M",
        contact: "+38654061588",
        admin: true,
        tk_id_location: 1,
        tk_id_image: 13
    },
    {
        name: "Filip",
        surname: "Mlakar",
        email: "filip@gmail.com",
        uid: 1111,
        gender: "M",
        contact: "+886927748",
        admin: false,
        tk_id_location: 3,
        tk_id_image: 12
    }
    ]
    const sport = [{
        name: "Yoga"
    },
    {
        name: "Hiking"
    },
    {
        name: "Cycling"
    },
    {
        name: "Running"
    },
    {
        name: "Pilates"
    },
    {
        name: "Basketball"
    },
    {
        name: "Volleybal"
    },
    {
        name: "Football"
    }
    ]
    const location = [
        { street: 'Ulica Pariške komune ', street_no: 10, city: 'Maribor' },
        { street: 'Koresova ulica', street_no: 10, city: 'Koper' },
        { street: 'Na poljah', street_no: 55, city: 'Maribor' },
        { street: 'Ulica heroja Šercerja', street_no: 13, city: 'Ljubljana' },
        { street: 'Ruška cesta', street_no: 40, city: 'Maribor' },
        { street: 'Cesta proletarskih brigad', street_no: 62, city: 'Maribor' },

    ]
    const comment = [{
        tk_user_id: 1,
        tk_event_id: 1,
        text: "Great event!",
        date: '2021-02-26'
    },
    {
        tk_user_id: "2",
        tk_event_id: 1,
        text: "Had so much fun!",
        date: '2021-02-27'
    },
    {
        tk_user_id: "1",
        tk_event_id: 3,
        text: "My muscles still hurt!",
        date: '2021-05-26'
    }
    ]
    const event = [{
        tk_id_location: 1,
        tk_id_sport: 1,
        tk_id_organizator: 1,
        capacity: 15,
        title: "Yoga retreat",
        date: '2022-07-26',
        tk_id_image:8,
        description: 'ARE YOU READY FOR A TRANSFORMATIVE YOGA RETREAT?'
    },
    {
        tk_id_location: 2,
        tk_id_sport: 4,
        tk_id_organizator: 3,
        capacity: 20,
        title: "Lets run together",
        date: '2022-08-08',
        tk_id_image:7,
        description:"Find a fun, friendly and supportive running group near you and join us. Running together helps you to monitor pace, cadence and breathing because there are people around you to refer to. We start at 9:00am."
    },
    {
        tk_id_location: 3,
        tk_id_sport: 6,
        tk_id_organizator: 2,
        capacity: 10,
        title: "Streetball",
        date: '2022-04-20',
        tk_id_image:2,
        description:"A unique form of the sport basketball, where most general rules are ignored and style is encouraged. Played outdoors, being the origin of the word. Join us at 19:00pm."
    },
    {
        tk_id_location: 2,
        tk_id_sport: 7,
        tk_id_organizator: 3,
        capacity: 8,
        title: "Beach volley with us",
        date: '2022-07-10',
        tk_id_image:10,
        description:"Since its beginnings, Beach Volleyball has gone hand in hand with music, fun in the stands and an atmosphere that relaxes both players and fans. WE GATHER AT 15:00!"
    },
    {
        tk_id_location: 5,
        tk_id_sport: 2,
        tk_id_organizator: 1,
        capacity: 10,
        title: "Always be at your peak - Climibing Triglav",
        date: '2022-07-21',
        tk_id_image:6,
        description:"Enjoy a thrilling mountaineering adventure and climb to the top of Mount Triglav, Slovenia's highest peak. We are meeting at 7:00am. SEE YOU!"
    },
    {
        tk_id_location: 6,
        tk_id_sport: 3,
        tk_id_organizator: 2,
        capacity: 12,
        title: "We just want a rollie rollie",
        date: '2022-06-14',
        tk_id_image:4,
        description:"Welcome to your calendar for the best organized cycling events near you! From Century Rides, Gran Fondos, and Charity Bike Tours, to Mountain Bike. YES, we are doing a MOUNTAIN BIKE TOUR! We are waiting for you at 7:30am."
    },
    {
        tk_id_location: 4,
        tk_id_sport: 5,
        tk_id_organizator: 3,
        capacity: 8,
        title: "Balanced Body Pilates",
        date: '2022-06-29',
        tk_id_image:11,
        description:"A beginner exercise class which concentrates on strengthening the body with an emphasis on core ... Join us at 19:00pm!"
    }
    ]

    const rating = [{
        tk_user_id: 1,
        grade: 4,
        tk_rater_uid:"1234"
    },
    {
        tk_user_id: 1,
        grade: 2,
        tk_rater_uid:"3241"
    },
    {
        tk_user_id: 1,
        grade: 5,
        tk_rater_uid:"1111"
    },
    {
        tk_user_id: 2,
        grade: 1,
        tk_rater_uid:"XkAXotJYmGMmJZBJFqNvPYxdEZ52"
    },
    {
        tk_user_id: 2,
        grade: 3,
        tk_rater_uid:"3241"
    },
    {
        tk_user_id: 2,
        grade: 3,
        tk_rater_uid:"1111"
    },
    {
        tk_user_id: 3,
        grade: 4,
        tk_rater_uid:"XkAXotJYmGMmJZBJFqNvPYxdEZ52"
    },
    {
        tk_user_id: 3,
        grade: 4,
        tk_rater_uid:"1234"
    },
    {
        tk_user_id: 3,
        grade: 5,
        tk_rater_uid:"1111"
    },
    {
        tk_user_id: 4,
        grade: 4,
        tk_rater_uid:"XkAXotJYmGMmJZBJFqNvPYxdEZ52"
    },
    {
        tk_user_id: 4,
        grade: 5,
        tk_rater_uid:"1234"
    },
    {
        tk_user_id: 4,
        grade: 5,
        tk_rater_uid:"3241"
    }
    ]

    const favourite = [{
        tk_user_id: 1,
        tk_sport_id: 1
    },
    {
        tk_user_id: 1,
        tk_sport_id: 2
    },
    {
        tk_user_id: 2,
        tk_sport_id: 1
    },
    {
        tk_user_id: 2,
        tk_sport_id: 4
    },
    {
        tk_user_id: 3,
        tk_sport_id: 3
    }]
    const event_has_users = [
      
        {
            tk_user_id: 2,
            tk_event_id: 1
        },
        {
            tk_user_id: 3,
            tk_event_id: 1
        },
        {
            tk_user_id: 1,
            tk_event_id: 2
        },
        {
            tk_user_id: 2,
            tk_event_id: 6
        },
        {
            tk_user_id: 2,
            tk_event_id: 5
        },
        {
            tk_user_id: 1,
            tk_event_id: 4
        },
        {
            tk_user_id: 2,
            tk_event_id: 2
        },
        {
            tk_user_id: 4,
            tk_event_id: 4
        },
        {
            tk_user_id: 2,
            tk_event_id: 3
        }
    ]

    const places = [{
        title: 'Nice place for yoga',
        tk_location_id: 1,
        tk_renter_id: 1,
        capacity: 15,
        price: 20,
        description: "The most beautiful urban temple Nicois, a magical place, a radiant team with a heart of gold.",
        date: '2021-02-26',
        tk_id_image: 9
    },
    {
        title: 'Small ballet studio',
        tk_location_id: 2,
        tk_renter_id: 2,
        capacity: 20,
        price: 10,
        description: "Only Studio that have theatrical lighting equipment available to enhance your photo/video shoot.",
        date: '2022-02-26',
        tk_id_image: 1
    },
    {
        title: 'Cozy studio for book club',
        tk_location_id: 3,
        tk_renter_id: 2,
        capacity: 10,
        price: 30,
        description: "The Book Club is a luxury self-catering experience in the heart of this vibrant and historic market town. ",
        date: '2023-02-26',
        tk_id_image: 3
    },
    {
        title: 'Rent a gym',
        tk_location_id: 4,
        tk_renter_id: 3,
        capacity: 10,
        price: 60,
        description: "The main gym can accommodate large meetings, conferences, and numerous indoor athletic activities including basketball and volleyball practices and games.",
        date: '2023-06-26',
        tk_id_image: 5
    }
    ]
    // FILL TABLES WITH INITAL VALUES
    await knex('sport').insert(sport).then(() => console.log("sports inserted")).catch((err) => { console.log(err); throw err });
    await knex('location').insert(location).then(() => console.log("locations inserted")).catch((err) => { console.log(err); throw err });
    await knex('images').insert(imgs).then(() => console.log("images for Events inserted")).catch((err) => { console.log(err); throw err });
    await knex('user').insert(user).then(() => console.log("users inserted")).catch((err) => { console.log(err); throw err });
    await knex('event').insert(event).then(() => console.log("events inserted")).catch((err) => { console.log(err); throw err });
    await knex('place').insert(places).then(() => console.log("places inserted")).catch((err) => { console.log(err); throw err });
    await knex('comment').insert(comment).then(() => console.log("comments inserted")).catch((err) => { console.log(err); throw err });
    await knex('favourite').insert(favourite).then(() => console.log("favourites inserted")).catch((err) => { console.log(err); throw err });
    await knex('rating').insert(rating).then(() => console.log("ratings inserted")).catch((err) => { console.log(err); throw err });
    await knex('event_has_users').insert(event_has_users).then(() => console.log("event_has_users inserted")).catch((err) => { console.log(err); throw err });

    knex.destroy();
}
fillDatabase();