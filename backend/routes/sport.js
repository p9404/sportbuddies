const express = require('express');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);

const sports = bookshelf.Model.extend({
  tableName: 'sport',
  idAttribute: 'sport_id'
});
/* GET Sports table. */

router.get('/', async function (req, res, next) {
  try {
    const allSports = await new sports().fetchAll();
    res.json(allSports.toJSON());
  }
  catch (error) {
    res.status(500).json({ status: "error", error: error });
  }
});
/* GET specific Sport */
router.get('/:sport_id', async (req, res, next) => {
  try {
    let specificSport = await new sports().where('sport_id', req.params.sport_id).fetchAll();
    res.json(specificSport.toJSON());
  } catch (error) {
    res.status(500).json({ status: "error", error: error });
  }
});

/* Post new Sports.*/
router.post('/', async (req, res, next) => {

  try {
    let sport1 = {
      name: req.body.name
    }
    let newSport = await new sports().save(sport1);
    res.json({ status: "added" });
  } catch (err) {
    res.status(500).json({ status: "error" });
    console.log(err);
  }
});
/*DELETE specific Comment */
router.delete('/:sport_id', async(req, res, next) => {
  try {
      const deletedSport = await new sports().where('sport_id', req.params.sport_id).destroy();
      res.json(deletedSport.toJSON());
  } catch (error) {
      res.status(500).json(error);
  }
});

module.exports = router;