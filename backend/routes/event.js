const express = require('express');
const { userInfo } = require('os');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);

const events = bookshelf.Model.extend({
    tableName: 'event',
    idAttribute: 'event_id'
});

const sports = bookshelf.Model.extend({
    tableName: 'sport',
    idAttribute: 'sport_id'
});

const locations = bookshelf.Model.extend({
    tableName: 'location',
    idAttribute: 'location_id'
});

const users = bookshelf.Model.extend({
    tableName: 'user',
    idAttribute: 'user_id'
});
const myFavourites = bookshelf.Model.extend({
    tableName: 'favourite',
    idAttribute: 'favourite_id'
});

/* GET Event table. */
router.get('/', async function (req, res, next) {
    try {
        const allEvents = await new events().fetchAll();
        var allEventsFilled = [];
        for (let i = 0; i < allEvents.toJSON().length; i++) {

            let location = await new locations().where('location_id', allEvents.toJSON()[i].tk_id_location).fetch();
            let sport = await new sports().where('sport_id', allEvents.toJSON()[i].tk_id_sport).fetch();
            let organizator = await new users().where('user_id', allEvents.toJSON()[i].tk_id_organizator).fetch();

            allEventsFilled.push({
                event: allEvents.toJSON()[i],
                location: location.toJSON(),
                sport: sport.toJSON(),
                organizator: organizator.toJSON()
            })

        }
        res.json(allEventsFilled);
    }
    catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});
/* GET specific Event */
router.get('/:event_id', async (req, res, next) => {
    try {
        let specificEvent = await new events().where('event_id', req.params.event_id).fetch();
        let specificLocation = await new locations().where('location_id', specificEvent.toJSON().tk_id_location).fetch();
        let specificSport = await new sports().where('sport_id', specificEvent.toJSON().tk_id_sport).fetch();
        let organizator = await new users().where('user_id', specificEvent.toJSON().tk_id_organizator).fetch();

        res.json({
            event: specificEvent.toJSON(),
            location: specificLocation.toJSON(),
            sport: specificSport.toJSON(),
            organizator: organizator.toJSON()
        });
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET Event by location */
router.get('/getLocation/:tk_id_location', async (req, res, next) => {
    try {
        const eventByLocation = await new events().where('tk_id_location', req.params.tk_id_location).fetchAll();
        res.json(eventByLocation.toJSON());
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET Event by organizator */
router.get('/byOrganizator/:tk_id_organizator', async (req, res, next) => {
    try {
        let eventByOrganizator = await new events().where('tk_id_organizator', req.params.tk_id_organizator).fetchAll();
        const organizator = await new users().where('user_id', req.params.tk_id_organizator).fetch().toJSON();
        var allEventsFilled = [];

        for (let i = 0; i < eventByOrganizator.toJSON().length; i++) {

            let location = await new locations().where('location_id', eventByOrganizator.toJSON()[i].tk_id_location).fetch();
            let sport = await new sports().where('sport_id', eventByOrganizator.toJSON()[i].tk_id_sport).fetch();

            allEventsFilled.push({
                event: eventByOrganizator.toJSON()[i],
                location: location.toJSON(),
                sport: sport.toJSON(),
                organizator: organizator
            })
        }

        res.json(allEventsFilled);
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET Event by sport */
router.get('/bySport/:tk_id_sport', async (req, res, next) => {
    try {
        let eventBySport = await new events().where('tk_id_sport', req.params.tk_id_sport).fetchAll();
        res.json(eventBySport.toJSON());
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET Events by sport type */
router.get('/bySportName/:sportName', async (req, res, next) => {
    try {
        let sportId = await new sports().where('name', req.params.sportName).fetch();
        let specificEvents = await new events().where('tk_id_sport', sportId.toJSON().sport_id).fetchAll();
        var allEventsFilled = [];
        for (let i = 0; i < specificEvents.toJSON().length; i++) {
            let location = await new locations().where('location_id', specificEvents.toJSON()[i].tk_id_location).fetch();
            let sport = await new sports().where('sport_id', specificEvents.toJSON()[i].tk_id_sport).fetch();
            let organizator = await new users().where('user_id', specificEvents.toJSON()[i].tk_id_organizator).fetch();

            allEventsFilled.push({
                event: specificEvents.toJSON()[i],
                location: location.toJSON(),
                sport: sport.toJSON(),
                organizator: organizator.toJSON()
            })
        }
        res.json(allEventsFilled);
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET capacity from event */
router.get('/capacity/:event_id', async (req, res, next) => {

    try {
        let capacity = await new events().where('event_id', req.params.event_id).fetch();
        res.json(capacity);
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET Event by favourite */
router.get('/byFavourites/:tk_user_id', async (req, res, next) => {
    try {
        const person = await new users().where('UID', req.params.tk_user_id).fetch();
        const favourite = await new myFavourites().where('tk_user_id', person.toJSON().user_id).fetchAll();
        const allEvents = await new events().fetchAll();
        let a = [];
        favourite.toJSON().map(f => a.push(f.tk_sport_id));
        let eventsByFavourite = [];
        for (let i = 0; i < allEvents.toJSON().length; i++) {

            if (a.find(a => a === allEvents.toJSON()[i].tk_id_sport)) {

                let location = await new locations().where('location_id', allEvents.toJSON()[i].tk_id_location).fetch();
                let sport = await new sports().where('sport_id', allEvents.toJSON()[i].tk_id_sport).fetch();
                let organizator = await new users().where('user_id', allEvents.toJSON()[i].tk_id_organizator).fetch();

                eventsByFavourite.push({
                    event: allEvents.toJSON()[i],
                    location: location.toJSON(),
                    sport: sport.toJSON(),
                    organizator: organizator.toJSON()
                })
            }

        }

        res.json(eventsByFavourite);
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* Post new Event.*/
router.post('/', async (req, res, next) => {
    const user = await new users().where('UID', req.body.useruid).fetch();
    try {
        let event1 = {
            tk_id_location: req.body.tk_id_location,
            tk_id_sport: req.body.tk_id_sport,
            tk_id_organizator: user.toJSON().user_id,
            capacity: req.body.capacity,
            title: req.body.title,
            description: req.body.description,
            date: req.body.date,
            tk_id_image:req.body.tk_id_image
        }
        let newLEvent = await new events().save(event1);
        res.json({
            status: "added",
            id: newLEvent.toJSON().event_id
        });
    } catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }
});

/*DELETE specific Event */
router.delete('/:event_id', async (req, res, next) => {
    try {
        const deletedEvent = await new events().where('event_id', req.params.event_id).destroy();
        res.json(deletedEvent.toJSON());
    } catch (error) {
        res.status(500).json(error);
    }
});

/* Change event info */
router.patch("/:event_id", async (req, res, next) => {
    try {
       console.log(req.body);
        if (req.body.tk_id_location != "")
            await new events({ event_id: req.params.event_id }).save({ tk_id_location: req.body.tk_id_location }, { patch: true });
        if (req.body.tk_id_sport != "")
            await new events({ event_id: req.params.event_id }).save({ tk_id_sport: req.body.tk_id_sport }, { patch: true });
        if (req.body.tk_id_image != "")
            await new events({ event_id: req.params.event_id }).save({ tk_id_image: req.body.tk_id_image }, { patch: true });
        if (req.body.capacity != "")
            await new events({ event_id: req.params.event_id }).save({ capacity: req.body.capacity }, { patch: true });
        if (req.body.title != "")
            await new events({ event_id: req.params.event_id }).save({ title: req.body.title }, { patch: true });
        if (req.body.description != "")
            await new events({ event_id: req.params.event_id }).save({ description: req.body.description }, { patch: true });
        if (req.body.date != "")
            await new events({ event_id: req.params.event_id }).save({ date: req.body.date }, { patch: true });
        res.json({ status: "patching" });
    }catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }

});



module.exports = router;