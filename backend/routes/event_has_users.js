const express = require('express');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);

const event_has_users = bookshelf.Model.extend({
    tableName: 'event_has_users',
    idAttribute: 'event_has_users_id'
});

const users = bookshelf.Model.extend({
    tableName: 'user',
    idAttribute: 'user_id'
});
const events = bookshelf.Model.extend({
    tableName: 'event',
    idAttribute: 'event_id'
});
const sports = bookshelf.Model.extend({
    tableName: 'sport',
    idAttribute: 'sport_id'
});

const locations = bookshelf.Model.extend({
    tableName: 'location',
    idAttribute: 'location_id'
});
/* GET Event has users table. */
router.get('/', async function (req, res, next) {
    try {
        const allEvent_has_users = await new event_has_users().fetchAll();
        res.json(allEvent_has_users.toJSON());
    }
    catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET check if user is on this event */
router.get('/:useruid/:event_id', async function (req, res, next) {

    try {
    const user1 = await new users().where('UID', req.params.useruid).fetch();
    const isUserOnEvent = await new event_has_users().where({
        tk_user_id: user1.toJSON().user_id,
        tk_event_id:  req.params.event_id}).fetchAll();
        //let isUserOnEvent = await new event_has_users().where('tk_user_id', user1.toJSON().user_id).fetchAll();
        if(isUserOnEvent.length > 0)
            res.json(true);
         else
            res.json(false);
    }
    catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* Post new user on event.*/
router.post('/:useruid', async (req, res, next) => {
        const user1 = await new users().where('UID', req.params.useruid).fetch();
    try {
        let event_has_users1 = {
            tk_user_id: user1.toJSON().user_id,
            tk_event_id: req.body.tk_event_id
        }
        await new event_has_users().save(event_has_users1);
        res.json({ status: "added" });
    } catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }
});
/* GET number of users joined the event. */
 router.get('/:event_id', async function (req, res, next) {

     try {
         const usersOnEvent = await new event_has_users().where({tk_event_id: req.params.event_id}).fetchAll();
         res.json(usersOnEvent.length);
     }
    catch (error) {
      res.status(500).json({ status: "error", error: error });
     }
 });

/* GET users on the event. */
router.get('/users/on_event/:event_id', async function (req, res, next) {
    try {
        const usersOnEvent = await new event_has_users().where({tk_event_id: req.params.event_id}).fetchAll();
        const userData = [];
        for(let i = 0; i < usersOnEvent.toJSON().length; i++){
            userData[i] = await new users().where({user_id: usersOnEvent.toJSON()[i].tk_user_id}).fetch();
        }
        //console.log(userData);
        res.json(userData); 
    }
   catch (error) {
     res.status(500).json({ status: "error", error: error });
    }
});


module.exports = router;