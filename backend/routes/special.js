const express = require('express');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);

const event_has_users = bookshelf.Model.extend({
    tableName: 'event_has_users',
    idAttribute: 'event_has_users_id'
});

const users = bookshelf.Model.extend({
    tableName: 'user',
    idAttribute: 'user_id'
});
const events = bookshelf.Model.extend({
    tableName: 'event',
    idAttribute: 'event_id'
});
const sports = bookshelf.Model.extend({
    tableName: 'sport',
    idAttribute: 'sport_id'
});

const locations = bookshelf.Model.extend({
    tableName: 'location',
    idAttribute: 'location_id'
});

/*GET all events a specific user joined.*/
router.get('/:tk_user_id', async function (req, res, next) {

    try {

        let allEvents = await new event_has_users().where({ tk_user_id: req.params.tk_user_id }).fetchAll();

        var allEventsFilled = [];
        var allInfo = [];
        for (let i = 0; i < allEvents.toJSON().length; i++) {

            let event = await new events().where('event_id', allEvents.toJSON()[i].tk_event_id).fetch();
            if( event.toJSON().date.getTime() > new Date().getTime() ){
                allEventsFilled.push(event.toJSON());

                let location = await new locations().where('location_id', allEventsFilled[i].tk_id_location).fetch();
                let sport = await new sports().where('sport_id', allEventsFilled[i].tk_id_sport).fetch();
                let organizator = await new users().where('user_id', allEventsFilled[i].tk_id_organizator).fetch();

                allInfo.push({
                    event: allEventsFilled[i],
                    location: location.toJSON(),
                    sport: sport.toJSON(),
                    organizator: organizator.toJSON()
                })
            }
        }
        res.json(allInfo);
    }
    catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

module.exports = router;