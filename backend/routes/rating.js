const express = require('express');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');
const connection = require('./imgdbcon');

const bookshelf = require('bookshelf')(knex);

const myRatings = bookshelf.Model.extend({
    tableName: 'rating',
    idAttribute: 'rating_id'
});

const users = bookshelf.Model.extend({
    tableName: 'user',
    idAttribute: 'user_id'
});

/* GET ALL ratings. */
router.get('/', async function (req, res, next) {
    try {
        const ratings = await new myRatings().fetchAll();
        res.json(ratings.toJSON());
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET ratings by user id */
router.get('/getRatings/:tk_user_id', async function (req, res, next) {
    try {

        const ratings = await new myRatings().where('tk_user_id', req.params.tk_user_id).fetchAll();

        var ratingDetails = [];

        for (let i = 0; i < ratings.toJSON().length; i++) {


            let user = await new users().where('user_id', ratings.toJSON()[i].tk_user_id).fetch();

            ratingDetails.push({
                rating: ratings.toJSON()[i],
                user: user.toJSON(),

            })

        }
        res.json(ratingDetails);

    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

router.post('/', async (req, res, next) => {
    try {
        let ratings = req.body;
        const add = await new myRatings().save(ratings);
        res.json({ status: "added" });
    } catch (error) {
        res.status(500).json(error);
    }
});

/* POST singular rating based on the user id */
router.post('/:useruid', async (req, res, next) => {

    //const user = await new users().where('UID',req.params.useruid).fetch();

    const ratingCheck = await new myRatings().where('tk_rater_uid', req.params.useruid).fetchAll();

    if (ratingCheck.toJSON().findIndex((rate) => rate.tk_user_id == req.body.user) === -1) {

        const rating = {
            grade: req.body.grade,
            tk_user_id: req.body.user,
            tk_rater_uid: req.params.useruid
        }

        await new myRatings().save(rating);

    } else {
        const sql = 'UPDATE rating SET grade = ? WHERE tk_user_id = ? AND tk_rater_uid = ?';
        connection.query(sql, [req.body.grade, req.body.user, req.params.useruid], (err, results) => {
            if (err) throw err;
            if (results[0]) {
                res.json({ success: 1, path: results[0].image })
            }
        });
    }
    res.json({ status: 200 })
});

module.exports = router;