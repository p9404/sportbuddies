var express = require('express');
var path = require('path');
var logger = require('morgan');
const cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var sportsRouter = require('./routes/sport');
var commentsRouter = require('./routes/comment');
var locationRouter = require('./routes/location');
var eventRouter = require('./routes/event');
var favouriteRouter = require('./routes/favourite');
var ratingRouter = require('./routes/rating');
var event_has_usersRouter = require('./routes/event_has_users')
var imgRouter = require('./routes/image');
var placeRouter = require('./routes/place');
var specialRouter = require('./routes/special');

const multer = require('multer');
var app = express();
/*
// Which port the server is being hosted on
var listener = app.listen(3030, function(){
    console.log('Listening on port ' + listener.address().port);
});
*/
//midleware
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.static(path.join(__dirname, './public_html')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/sport', sportsRouter);
app.use('/comment', commentsRouter);
app.use('/location', locationRouter);
app.use('/event', eventRouter);
app.use('/favourite', favouriteRouter);
app.use('/rating', ratingRouter);
app.use('/event_has_users', event_has_usersRouter);
app.use('/image', imgRouter);
app.use('/place', placeRouter);
app.use('/special', specialRouter);

module.exports = app;
