# Praktikum 2 - Sport buddies

We made an app, that first of all, we though that we need nowadays. While making it we were improving our programming skills that we acquired this year and at the same we were thinking about helping our community to be more active. It goes for a platform which provides events of all types, that can be added by registered users. 

## How to use
After registering, on our home page are shown all the events people shared. Also, you, as a registered user, have an option to add an event of any kind. With our search bar you can easily navigate through all the events, either by their name, location or sport. Also, the important thing is that, on your profile, you have an option to add your favourite sport, and then in the favourites section you can see all the events of that sport. 

### How can you join an event ? 
By clicking to see the details of the event, all information you need are shown. You can also see the capacity of the event and how much users have joined so far. If there is still room for you, you can join by clicking the join button, which will increase the number of joined people. If you have chosen an event that had happened already, you are not allowed to join. Also, you can add comments to the events, before or after they happen. 
After joining a particular event, you will see it in the upcoming events section on the home page. 
For all indoor sports, like yoga, pilates etc.. we made a section where you can share your place for renting, like for example, a forum, where you can add all needed information about it, and your contact can be found on your profile, for anyone who is interested. You can also add comments to places that are available to rent.

### Profiles

On your profile, you can update your picture and your other information. You can also find your average rating. 
Besides your profile, you can also see other users profiles, where you will find all events they had hosted and all places they had rented. Also, you can rate other users by entering a grade from 1-5. 

## Requirements
1) Having WAMP 
2) Visual Studio Code ( [Windows](https://code.visualstudio.com/docs/setup/windows#:~:text=Download%20the%20Visual%20Studio%20Code,%5CPrograms%5CMicrosoft%20VS%20Code%20.) | [Mac](https://code.visualstudio.com/docs/setup/mac) | [Linux](https://code.visualstudio.com/docs/setup/linux))
3) MySql
4) [Node](https://kinsta.com/blog/how-to-install-node-js/)
5) Having [Git Installed](https://www.atlassian.com/git/tutorials/install-git) on your computer, logged into your GitLab account and [connected to your Visual Studio Code](https://learn.microsoft.com/en-us/visualstudio/get-started/tutorial-open-project-from-repo?view=vs-2022)

## Installation

### For Windows Users

1. Open PHPMyAdmin panel, log onto your user, create database Praktikum2.  
2. Clone the repository master from gitLab (Visual Studio Code (HTTPS)).  
3. Under routes you will find 2 files : dbcon.js and imgdbcon.js. Configure the connection to fit the user (if you have a password or are using a different user other than root).  
4. Open a new terminal in VS Code  
5. Move to the backend directory (cd backend)  
6. Run `npm i`  
7. Run `node CreateTables.js`  
8. Run `npm start`  
9. Open another terminal in VS Code  
10. Move to the frontend directory (cd frontend)  
11. Run `npm i`  
12. Run `npm start`  

And you are ready!  

### Prerequisites for Linux Users
Ubuntu Server 18.04 (or higher).
Root or sudo access on the server.
The server must have the Linux, Apache, MySQL, PHP (LAMP) stack installed
    Since Linux doesn't have WAMP the best alternative is [LAMP](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-ubuntu-18-04)

### For Mac Users
    Since Mac doesn't have Wampserver the best alternative to use is [XAMPP](https://www.javatpoint.com/how-to-install-xampp-on-mac)
1. Start the XAMPP server through it's control panel
2. Turn on Apache and MySql
3. Click on the Admin button next to the MySql running server
4. Log onto your user, create database Praktikum2.  
5. Clone the repository master from gitLab (Visual Studio Code (HTTPS)).  
6. Under routes you will find 2 files : dbcon.js and imgdbcon.js. Configure the connection to fit the user (if you have a password or are using a different user other than root).  
7. Open a new terminal in VS Code  
8. Move to the backend directory (cd backend)  
9. Run `npm i`  
10. Run `node CreateTables.js`  
11. Run `npm start`  
12. Open another terminal in VS Code  
13. Move to the frontend directory (cd frontend)  
14. Run `npm i`  
15. Run `npm start`  

And you are ready!  

## Software Stack
 - **React** fronted framework coded in **Typescript** with `.tsx` files 
 - **Firebase Auth** implementation for user authentication
 - **Express** implemented within **Node.js** as the backend framework
 - **GitLab** as the kanban and CD platform

## License
**Copyright __2022__ Sport Buddies**

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

<sub>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</sub>

## Functionalities
- Add an event
- Commenting on the event
- Edit your events
- Search for an event by location, sport, or name
- Joining events
- View your upcoming events
- Add a place that can be rented
- Adding favorite sports
- View all events from selected favorites
- View organizer's profile
- Organizer rating
- View your profile
- Edit your profile
- View all the events you've hosted
- View all the events you've joined
- See all the places you rent.

## Authors
Olga Ivkovic, Ivan Sergejev, Dimitrije Stefanovic, Veronija Kodovska

## Screenshots
![Landing page](https://media.discordapp.net/attachments/984370797583933441/984379721896841246/2.PNG?width=1308&height=604 "Landing page")
![Adding event modal](https://media.discordapp.net/attachments/984370797583933441/984379722429526036/4.PNG?width=368&height=604 "Adding event modal")
![User profile](https://media.discordapp.net/attachments/984370797583933441/984379722156883969/3.PNG?width=1202&height=604 "User profile")
![Favorites section](https://media.discordapp.net/attachments/984370797583933441/984379722706354206/5.PNG "Favorites sector")
![Home page](https://cdn.discordapp.com/attachments/984370797583933441/984478201927589888/unknown.png "Home page")
