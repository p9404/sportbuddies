export interface Sport {
    sport_id: number;
    name: string;
}

export interface Location {
    location_id: number;
    city: string;
    street: string;
    street_no: string;
}

export interface Event {
    event_id: number;
    tk_location_id: number;
    tk_sport_id: number;
    tk_id_organizator: number;
    capacity: number;
    title: string;
    description: string;
    date: string;
    tk_id_image: number;
}

export interface User {
    user_id: number;
    name: string;
    surname: string;
    email: string;
    UID: string;
    gender: string,
    contact: string,
    admin: boolean;
    tk_id_location: number;
    tk_id_image: number;
    description: string;
}

export interface Place {
    place_id: number;
    tk_renter_id: number;
    tk_location_id: number;
    tk_id_image: number;
    title: string;
    description: string;
    capacity: number;
    price: number;
    date: Date;
}

export interface Comment {
    comment_id: number;
    date: Date;
    text: string;
    tk_event_id: number;
    tk_user_id: number;
}

export interface Rating {
    rating_id: number,
    grade: number,
    tk_user_id: number
}

export interface Place {
    place_id: number;
    tk_location_id: number;
    tk_renter_id: number;
    title: string;
    capacity: number;
    price: number;
    description: string;
    date: Date;
    tk_id_image: number;
}