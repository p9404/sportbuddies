import React, { useState } from 'react'
import { Checkbox, Collapse } from 'antd';
import { Sport } from '../classes';
import { Form } from 'react-bootstrap';

interface props{
    sports?: Sport[],
    handleFilters: (filters: any) => any
}

function CheckBoxSport(props:props) {

    const [Checked, setChecked] = useState(Array<number>())

    const handleToggle = (value: number) => {

        const currentIndex = Checked.indexOf(value);
        const newChecked = [...Checked];

        if (currentIndex === -1) {
            newChecked.push(value)
        } else {
            newChecked.splice(currentIndex, 1)
        }

        setChecked(newChecked)
        props.handleFilters(newChecked)
        //update this checked information into Parent Component 

    }

    const renderCheckboxLists = () => props.sports && props.sports.map((value, index) => (
        <React.Fragment>
            <Form.Check
                onChange={() => handleToggle(value.sport_id)}
                checked={Checked.indexOf(value.sport_id) === -1 ? false : true}
                key={value.sport_id}
            />
            <span className='mr-4 ml-1'>{value.name}</span>
        </React.Fragment>
    ))

    return (
        <div className='mx-auto my-2 flex'>
            {renderCheckboxLists()}
        </div>
    )
}

export default CheckBoxSport
