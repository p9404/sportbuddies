import axios from "axios";
import { useEffect, useState } from "react";
import { Button, Card, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Event, Location, Place, Sport, User } from "../classes";

interface props{
    eventdetail: {
        event: Event;
        location: Location;
        sport: Sport;
        organizator: User;
    }
}

export default function EventCard(props:props){
    
    const [attendees, setAttendees] = useState("");
    const [placeImagePath, setPlaceImgagePath] = useState("");

    function nadjiSliku() {
        axios.get(`image/${props.eventdetail.event.tk_id_image}`)
            .then((response) => {
                    setPlaceImgagePath('http://localhost:3030/'+response.data.path);
            })
    }

    function postUserToEvent(){
        axios.get('/event_has_users/'+ props.eventdetail.event.event_id).then((response)=>{
            setAttendees(response.data);
        }).catch((error)=>{
            console.log(error);  
        })
    }

    useEffect(() => {
        postUserToEvent();
        nadjiSliku();
    },[])
    
    return(
        <Col className="mb-3">
            <Card style={{height:"25rem"}}>
                <Card.Img className="object-cover h-[5rem]" variant="top" src={placeImagePath || ""} />
                <Card.Body className="flex flex-col">
                    <div className="mb-auto">
                        <Card.Title>{props.eventdetail.event.title} <Link to={'event/'+props.eventdetail.event.event_id.toString()} className="text-muted fs-6 text-decoration-none"> ({attendees} / {props.eventdetail.event.capacity}) </Link> </Card.Title>
                        <Card.Text>
                            {props.eventdetail.event.description || "Some description about the event (it is currently missing)"}
                        </Card.Text>
                    </div>
                    <Link to={'/user/event/'+props.eventdetail.event.event_id.toString()}><Button variant="purple">See the event</Button></Link>
                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">Join us on : {Intl.DateTimeFormat(['ban', 'id']).format(new Date(props.eventdetail.event.date))}</small>
                </Card.Footer>
            </Card> 
        </Col>
    )
}