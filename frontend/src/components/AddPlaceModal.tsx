import axios from "axios";
import React from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { auth } from "../firebase-config";

interface Props {
  show: boolean;
  handleClose: () => void;
  osvezi: (a: boolean) => void;
}

export default function AddPlaceModal(props: Props) {
  const [idLocation, setIdLocation] = React.useState(0);
  const [validated, setValidated] = React.useState(false);
  const [invalidDate, setInvalidDate] = React.useState(false);

  const [userInfo, setuserInfo] = React.useState({
    file: new Blob(),
    filepreview: "",
    lol: false,
  });

  let o1 = false;

  const handleInputChange = (event: any) => {
    setuserInfo({
      ...userInfo,
      file: event.target.files[0],
      filepreview: URL.createObjectURL(event.target.files[0]),
      lol: true,
    });
  };

  const doSomething = (event: any) => {
    event.preventDefault();
    setValidated(true);
    if (event.currentTarget.checkValidity() === true) {
      if (new Date(event.target.dob.value) < new Date()) {
        setInvalidDate(true);
      } else {
        axios
          .post("/location", {
            street: event.target.street.value,
            street_no: event.target.streetNo.value,
            city: event.target.city.value,
          })
          .then((res) => {
            if (res.status == 200) {
              setIdLocation(res.data.id);
              const formdata = new FormData();
              if (userInfo.filepreview !== "") {
                formdata.append("avatar", userInfo.file);
                axios
                  .post(`image/imageupload`, formdata, {
                    headers: { "Content-Type": "multipart/form-data" },
                  })
                  .then((res2) => {
                    // then print response status
                    console.warn(res2);
                    if (res2.data.success === 1) {
                      AddPlace(res.data.id, res2.data.image_id);
                    }
                  });
              } else {
                AddPlace(res.data.id, 16);
              }
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    }

    function AddPlace(lokacija: number, picture: number) {
      axios
        .post("/place", {
          tk_id_location: lokacija,
          useruid: auth.currentUser?.uid,
          capacity: parseInt(event.target.formCapacity.value),
          price: parseInt(event.target.formPrice.value),
          title: event.target.formTitle.value,
          description: event.target.formDescription.value,
          date: event.target.dob.value,
          tk_id_image: picture,
        })
        .then((res) => {
          if (res.status == 200) {
            o1 = true;
            //props.osvezi(o1);
            window.location.reload();
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  return (
    <Modal show={props.show} onHide={props.handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Share a place for renting</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form
          className="flex flex-col gap-4"
          onSubmit={doSomething}
          noValidate
          validated={validated}
        >
          <Form.Group controlId="formTitle">
            <Form.Label>Name of the place</Form.Label>
            <Form.Control
              type="text"
              name="title"
              placeholder="Enter title"
              required
            />
            <Form.Control.Feedback type="invalid">
              Please provide a valid name.
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group className="flex flex-col gap-2">
            <Form.Label>Enter location info</Form.Label>
            <Form.Control type="text" id="city" placeholder="City" required />
            <Form.Control
              type="text"
              id="street"
              placeholder="Street"
              required
            />
            <Form.Control
              type="text"
              id="streetNo"
              placeholder="Street no."
              required
            />
            <Form.Control.Feedback type="invalid">
              Please provide a location for your place.
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group controlId="formDescription">
            <Form.Label>Description</Form.Label>
            <Form.Control as="textarea" />
          </Form.Group>
          <Form.Group controlId="formCapacity">
            <Form.Label>Estimated number of people</Form.Label>
            <Form.Control type="number" required />
            <Form.Control.Feedback type="invalid">
              Please provide capacity in number of people.
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group controlId="formPrice">
            <Form.Label>Price in €</Form.Label>
            <Form.Control type="number" required />
            <Form.Control.Feedback type="invalid">
              Please provide a price for your rental place.
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group controlId="dob">
            <Form.Label>Select Date</Form.Label>
            <Form.Control type="date" name="dob" placeholder="Date" required />
            <Form.Control.Feedback type="invalid">
              Please select a date for renting.
            </Form.Control.Feedback>
            {invalidDate === true && (
              <Form.Control.Feedback>
                <p className="text-danger">
                  The date you provided is not correct
                </p>
              </Form.Control.Feedback>
            )}
          </Form.Group>
          <Form.Group>
            <div className="flex justify-center items-center w-full">
              {userInfo.lol ? (
                <img
                  className="previewimg"
                  src={userInfo.filepreview}
                  alt="UploadImage"
                />
              ) : (
                <label
                  htmlFor="dropzone-file"
                  className="flex flex-col justify-center items-center w-full h-64 bg-gray-50 rounded-lg border-2 border-gray-300 border-dashed cursor-pointer dark:hover:bg-bray-100 hover:bg-gray-100 dark:hover:border-gray-100 dark:hover:bg-gray-100"
                >
                  <div className="flex flex-col justify-center items-center pt-5 pb-6">
                    <svg
                      className="mb-3 w-10 h-10 text-gray-400"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
                      ></path>
                    </svg>
                    <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                      <span className="font-semibold">Click to upload</span> or
                      drag and drop
                    </p>
                    <p className="text-xs text-gray-500 dark:text-gray-400">
                      SVG, PNG, JPG or GIF (MAX. 800x400px)
                    </p>
                  </div>
                  <input
                    id="dropzone-file"
                    type="file"
                    className="hidden"
                    onChange={handleInputChange}
                  />
                </label>
              )}
            </div>
          </Form.Group>
          <Button variant="gradient" type="submit">
            Submit
          </Button>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="outline-secondary"
          onClick={props.handleClose}
          className="w-full"
        >
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
