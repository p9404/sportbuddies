import { Outlet } from "react-router-dom";
import UserNav from "./UserNav";

export default function WrapperAuth(){
    return(
        <div className="wrapper-container">
            <UserNav/>
            <main className="flex">
                <Outlet/>
            </main>
            <footer className="text-muted border-top mx-5 py-2">
                    Praktikum II 2022 | V.O.I.D.
            </footer>
        </div>
    )
}