import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';



const firebaseConfig = {
    apiKey: "AIzaSyDp9zvB3SwKRjHwVzEMoodAA3lv_V7vdas",
    authDomain: "fir-auth-68c95.firebaseapp.com",
    projectId: "fir-auth-68c95",
    storageBucket: "fir-auth-68c95.appspot.com",
    messagingSenderId: "695140348181",
    appId: "1:695140348181:web:3465008c3c171591fa791c",
    measurementId: "G-WGXW1V4J4L"
};

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);