import {BrowserRouter, Route, Routes} from "react-router-dom";

import Wrapper from './components/Wapper';
import WrapperUser from './components/WrapperUser';
import Landing from './pages/Landing';
import  Events from './pages/Events';
import Login from './pages/Login';
import Register from './pages/Register';
import SpecificEvent from './pages/SpecificEvent';
import Favourites from "./pages/Favourites";
import SpecificSport from "./pages/SpecificSport";

import Profile from "./pages/Profile";

import "./styles.css"
import ViewProfile from "./pages/ViewProfile";
import EventsByFavourite from "./pages/EventsByFavourite";
import Places from "./pages/Places";
import SpecificPlace from "./pages/SpecificPlace";
import EditProfile from "./pages/EditProfile";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Wrapper/>}  /** Put guest routes here */>
          <Route index element={<Landing/>}/>
          <Route path='login' element={<Login/>}/>
          <Route path='register' element={<Register/>}/>
          <Route path='event' element={<Events/>}/>
        </Route>
        <Route path='user' element={<WrapperUser/>} /** Put authenticated routes here */ >
          <Route index element={<Events/>}/>
          <Route path='place' element={<Places/>}/>
          <Route path='place/:placeId' element={<SpecificPlace/>}/>
          <Route path='event/:eventId' element={<SpecificEvent/>}/>
          <Route path='sport/:sportName' element={<SpecificSport/>}/>
          <Route path='favourites' element={<Favourites/>}/>
          <Route path='events-by-favourites' element={<EventsByFavourite/>}/>
          <Route path='profile' element={<Profile/>}/>
          <Route path='profile/:userId' element={<ViewProfile/>}/>
          <Route path='editProfile/:userId' element={<EditProfile/>}/>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
