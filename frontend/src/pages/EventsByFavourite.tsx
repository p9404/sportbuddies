import axios from "axios"
import { onAuthStateChanged } from "firebase/auth";
import React, { useState } from "react";
import { Container, Row } from "react-bootstrap";
import { Event, Sport, Location, User } from "../classes";
import { auth } from "../firebase-config";
import EventCard from "../components/EventCard";
import CheckBoxSport from "../components/CheckBoxSport";

interface EventDetail {
    event: Event;
    sport: Sport;
    location: Location;
    organizator: User;
}

export default function SpecificEvent() {
    const [event, setEvent] = React.useState(Array<EventDetail>());
    const [uid, setUid] = useState("");
    const [message, setMessage] = React.useState("");
    let d;

    React.useEffect(() => {
        if (uid != "") {
            axios.get(`event/byFavourites/${uid}`)
                .then((response) => {
                    if (response.data.length !== 0) {
                        setEvent(response.data);
                        setMessage("All events based on your favourite sports")
                    }
                    else {
                        setEvent(response.data);
                        setMessage("No favourite sports added.")
                    }
                })
        }
    }, [uid])

    onAuthStateChanged(auth, (user) => {
        if (user) {
            setUid(user.uid);
        } else { }
    });
    return (
        <>
            <div className="my-5 w-75 mx-auto mb-auto bordered">
                <h3 className="bg-gradient-to-br from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold border-bottom pb-2 mx-4">{message}</h3>
                <Container className="mt-4 px-4">
                    <Row xs={1} sm={1} md={2} xl={3}>
                        {event.map(res => <EventCard eventdetail={res} key={res.event.event_id} />)}
                    </Row>
                </Container>
            </div>
        </>
    )
}