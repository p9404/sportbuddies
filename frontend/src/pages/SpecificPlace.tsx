import axios from "axios"
import { onAuthStateChanged } from "firebase/auth";
import React, { useState } from "react";
import { Button, Card, Col, OverlayTrigger, Row, Tooltip } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { Location, User, Place } from "../classes";
import { auth } from "../firebase-config";

interface SpecificPlace {
    place: Place;
    location: Location;
    renter: User;
}

export default function SpecificPlace() {

    const { placeId } = useParams<{ placeId: string }>();
    const [specific, setSpecific] = useState<SpecificPlace>();
    const [placeImagePath, setPlaceImgagePath] = useState<String>();
    const [profileImagePath, setProfileImgagePath] = useState<String>();
    const [isSameProfile, setIsSameProfile] = React.useState('');
    const [userUID, setUserUID] = useState("");
    const [uid, setUid] = useState("");
    let d;

    React.useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                setUid(user.uid);
            } else {
                // User not logged in or has just logged out.
            }
        });
    }, [])

    React.useEffect(() => {
        axios.get(`place/${placeId}`)
            .then((response) => {
                setSpecific(response.data);
                d = response.data.place.date;
                setUserUID(response.data.renter.UID);
                nadjiSliku(response.data.place.tk_id_image, 'place');
                nadjiSliku(response.data.renter.tk_id_image, 'pfp');
                if (response.data.renter.UID == uid) {
                    setIsSameProfile('/user/profile/');
                } else setIsSameProfile('/user/profile/' + response.data.renter.UID);
            })
    }, [uid])

    function nadjiSliku(idPic: number, type: string) {
        axios.get(`image/${idPic}`)
            .then((response) => {
                if (type === 'place') {
                    setPlaceImgagePath(response.data.path);
                } else if (type === 'pfp') {
                    setProfileImgagePath(response.data.path);
                }
            })
    }

    let datePlace = Intl.DateTimeFormat(['ban', 'id']).format(d);

    return (
        <>
            <div className="p-4 my-5 w-75 mx-auto bordered">
                <h1 className="text-center bg-gradient-to-tr from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold">{specific?.place.title}</h1>
                <Row gutter={[16, 16]} >
                    <Col lg={12} xs={24}>
                        <Card className="my-3 mx-auto !border-0" style={{ maxWidth: "960px" }}>
                            {placeImagePath ?
                                <Card.Img style={{ maxHeight: "180px", objectFit: "contain" }} className="mx-auto" variant="top" alt={specific?.place.title} src={'http://localhost:3030/' + placeImagePath} />
                                :
                                <Card.Img style={{ maxHeight: "180px", objectFit: "contain" }} className="mx-auto" variant="top" alt={specific?.place.title} src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22960%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20960%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_180f979480f%20text%20%7B%20fill%3A%23999%3Bfont-weight%3Anormal%3Bfont-family%3Avar(--bs-font-sans-serif)%2C%20monospace%3Bfont-size%3A48pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_180f979480f%22%3E%3Crect%20width%3D%22960%22%20height%3D%22180%22%20fill%3D%22%23373940%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22361.80833435058594%22%20y%3D%22115.5%22%3E960x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" />}
                            <Card.Body>
                                <Card.Text className="border-t-2 pt-1">
                                    Enjoy the luxury by {specific?.renter.name} in {specific?.location.city}!
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col lg={12} xs={24}>
                        <div className="my-2 mx-auto flex flex-col gap-2" style={{ maxWidth: "960px" }}>
                            <h5>
                                <i> {specific?.place.title} </i> is available for renting
                            </h5>
                            <h6 className="-mt-2">
                                <i>
                                    {specific?.location.city}, {specific?.location.street} {specific?.location.street_no}
                                </i>
                            </h6>
                            <div className="text-muted -mt-3">
                                From {datePlace}
                            </div>
                            <div>
                                {specific?.place.description || "Some description about the place (it is currently missing)"}
                            </div>
                            <div className="text-muted">
                                Intended for about {specific?.place.capacity} people
                            </div>
                            <OverlayTrigger
                                placement='right'
                                overlay={<Tooltip id={`tooltip-right`}>
                                    {specific?.renter.name}
                                </Tooltip>}
                            >
                                {profileImagePath ?
                                    <Link to={isSameProfile} className="w-fit"><img className="rounded-circle" style={{ width: "48px", height: "48px" }} src={'http://localhost:3030/' + profileImagePath} /></Link>
                                    :
                                    <Link to={isSameProfile} className="w-fit"><img className="rounded-circle" style={{ width: "48px", height: "48px" }} src={'http://localhost:3030/blank-profile-picture-973460_1280.png'} /></Link>}
                            </OverlayTrigger>
                        </div>
                        <div className="my-2 mx-auto flex flex-col border rounded-lg p-2 mt-3" style={{ maxWidth: "960px" }}>
                            <h2 className="mx-auto bg-gradient-to-tr from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold border-b">Get in contact</h2>
                            <div className="flex">
                                <h4 className="font-medium ml-2 mr-auto">{specific?.renter.name} {specific?.renter.surname}</h4>
                                <span className="text-muted mx-2">Starting at: {specific?.place.price}€ per day</span>
                            </div>
                            <div className="flex flex-wrap m-2">
                                <span className="text-muted mr-auto"> Get in contact with the user to see if the place is available on your desired date : </span>
                                <a className="logout" href={'mailto:' + specific?.renter.email}>{specific?.renter.email}</a>
                            </div>
                            <div className="flex flex-wrap m-2">
                                <span className="text-muted mr-auto"> Or try another way : </span>
                                <a className="logout cursor-pointer">{specific?.renter.contact}</a>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        </>
    )
}