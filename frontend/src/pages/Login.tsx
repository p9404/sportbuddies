import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Stack from "react-bootstrap/Stack";
import { Link } from 'react-router-dom';
import { signInWithEmailAndPassword, onAuthStateChanged } from 'firebase/auth';
import { auth } from '../firebase-config';
import { Card } from 'react-bootstrap';


// https://react-bootstrap.github.io/forms/overview/
export default function Login(){
  
   
    const [loginEmail, setLoginEmail] = useState("");
    const [loginPassword, setLoginPassword] = useState("");
    const [validated, setValidated] = useState(false);

    const [user, setUser] = useState({});

    const login = async () => {
        
        try {
            setValidated(true);
          const user = await signInWithEmailAndPassword
          (
            auth,
            loginEmail,
            loginPassword
          );

          onAuthStateChanged(auth, (currentUser) => {
            if(currentUser != null)
                setUser(currentUser);
            });

        } 
        catch (error) {
          console.log("error");
        }
    };

    return(
        <Stack gap={2} className="col-lg-3 col-md-4 col-8 m-auto">
            <div className="text-center bg-gradient-to-br from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold text-3xl px-2 pb-1">
                Log in with your account,
            </div>
            <div className='text-muted text-center mb-2 -mt-2'>and join all the events you can think of</div>
            
            <Form noValidate validated={validated} className="w-75 mx-auto">
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" 
                        onChange={
                            (event)=> {setLoginEmail(event.target.value)}
                        } 
                        required/>
                    <Form.Control.Feedback type="invalid">
                        Please provide a valid email.
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={
                        (event)=> {setLoginPassword(event.target.value)}
                        } required/>
                        <Form.Control.Feedback type="invalid">
                        Please provide a valid password.
                    </Form.Control.Feedback>
                </Form.Group>
                
                <div className="mx-auto flex flex-wrap gap-2 justify-center mt-5">
                    <Button variant="gradient" onClick={login}>
                        Log in
                    </Button>
                    <Link to="/register" className='btn btn-outline-purple' >
                        Register
                    </Link>
                </div>
                <img src={require('../images/logo.png')} alt="Sites logo" className="w-48 sm:w-32 md:w-48 lg:w-64 mx-auto my-5" />
            </Form>
        </Stack> 
    )
}

export {auth};