import axios from "axios"
import { onAuthStateChanged } from "firebase/auth";
import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Event, Sport, Location } from "../classes";
import { auth } from "../firebase-config";

export default function Favourites() {


    const [fav, setFav] = useState<Sport[]>();
    const [selectedSport, setSelectedSport] = useState(0);
    const [sport, setSport] = React.useState(Array<Sport>());

    const [uid, setUid] = useState("");

    React.useEffect(() => {
        onAuthStateChanged(auth, (user) => { // TODO
            if (user) {
                setUid(user.uid);
            } else {
                // User not logged in or has just logged out.
            }
        });
    },[])

    React.useEffect(() => {
        if (uid != "") {
            getSports();
            getFavs();
        }
    }, [uid])

    const getFavs = () => {
        axios.get(`favourite/${auth.currentUser?.uid}`)
            .then((response) => {
                setFav(response.data);
            })
    }

    const getSports = () => {
        axios.get("sport")
            .then((response) => {
                setSport(response.data);
            }).catch(error => {
                alert("something wrong")
            })
    }

    const addToFavs = (sport: number) => {
        let obj = fav?.find(o => o.sport_id === sport);
        if (!fav?.find(o => o.sport_id === sport)) {
            axios.post('favourite/', {
                tk_user_id: uid,
                tk_sport_id: sport
            }).then((response) => {
                if (response.status == 200) {
                    getFavs();
                }
            }).catch((error) => {
                console.log(error);
            })
        }
    }

    const deleteFav = (sport: number) => {
        axios.delete(`favourite/${uid}/${sport}`).then((response) => {
            if (response.status == 200) {
                getFavs();
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    return (
        <div className="p-4 w-50 mx-auto bordered">
            <div className="border-b-2 pb-1 mb-3 bg-gradient-to-br from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold text-lg">
                Select your favorite sports! 
            </div>
            <div className="flex gap-2 w-full">
                    <Form.Select value={selectedSport} onChange={(e) => setSelectedSport(parseInt(e.target.value))}>
                        {sport.map((o) => {
                            const { name, sport_id } = o;
                            return <option value={sport_id} key={sport_id}>{name}</option>;
                        })}
                    </Form.Select>
                    <Button variant="gradient" onClick={() => addToFavs(selectedSport)}>Favorite</Button>
            </div>
            <div className="mt-4">
                <div className="border-b-2 py-1 mb-3 bg-gradient-to-br from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold"> 
                    My Favorites 
                </div>
                <div className="flex flex-col gap-2">
                    {fav?.map(res =>
                        <div className="flex" key={res.sport_id}>
                            <span className="mr-auto"> {res.name} </span>
                            <Button variant="outline-purple" onClick={() => deleteFav(res.sport_id)}>Remove from favorites</Button>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}