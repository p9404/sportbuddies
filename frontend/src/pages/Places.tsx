import axios from "axios"
import React from "react";
import { Container, Row } from "react-bootstrap";
import Button from "react-bootstrap/esm/Button";
import { Location, Place, Sport, User } from "../classes";
import AddPlaceModal from "../components/AddPlaceModal";
import PlaceCard from "../components/PlaceCard";

interface PlaceDetail {
    place: Place;
    location: Location;
    sport: Sport;
    renter: User;
}

export default function Places() {

    const [isModalOpen, setModalState] = React.useState(false);
    const [place, setPlace] = React.useState(Array<PlaceDetail>());
    const [temp, setTemp] = React.useState(Array<PlaceDetail>());
    const [searchTerm, setSearchTerm] = React.useState("")

    React.useEffect(() => {
        getPlaces();
    }, [])

    const getPlaces = () => {
        axios.get("place")
            .then((response) => {
                setPlace(response.data);
                setTemp(response.data);
            })
    }

    const osvezi = (a: boolean) => {
        setModalState(!isModalOpen);
        getPlaces();
    }

    const handleHideModal = () => setModalState(false);
    const handleShowModal = () => setModalState(true);

    return (
        <div className="mx-2 mb-auto flex flex-col">
            <div className="input-group mt-3 w-50 mx-auto">
                <input type="text" className="form-control" placeholder="Enter name of an place you're searching for" onChange={event => { setSearchTerm(event.target.value) }}></input>
            </div>
            <Button variant="gradient" className="mx-auto mt-3 w-25" onClick={handleShowModal}>Share a place for renting</Button>
            <AddPlaceModal
                show={isModalOpen}
                handleClose={handleHideModal}
                osvezi={(o1: boolean) => osvezi(o1)}
            />
            <Container className="mt-4 border border-bottom-0 border-1 rounded-1 px-4 pt-4">
                <Row xs={1} sm={1} md={2} xl={3}>
                    {place.filter((res) => {

                        if (searchTerm == "") {
                            return res;
                        } else if (res.place.title.toLocaleLowerCase().includes(searchTerm.toLocaleLowerCase())) {
                            return res;
                        } else if (res.location.city.toLocaleLowerCase() === (searchTerm.toLocaleLowerCase())) {
                            return res;
                        }
                        else {
                            return 0;
                        }

                    }).map(res => <PlaceCard placedetail={res} key={res.place.place_id} />)
                    }
                </Row>
            </Container>
        </div>
    )
}
