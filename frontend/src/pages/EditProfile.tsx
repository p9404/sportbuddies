import { Button, FloatingLabel, Form } from "react-bootstrap";
import {
  CalendarIcon,
  OfficeBuildingIcon,
  UserIcon,
} from "@heroicons/react/outline";
import { Link, useNavigate } from "react-router-dom";
import { auth } from "../firebase-config";
import { onAuthStateChanged } from "firebase/auth";
import { useState } from "react";
import { Event, Location, Sport, User } from "../classes";
import axios from "axios";
import React from "react";

interface UserDetails {
  user: User;
  location: Location;
}

export default function Profile() {
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [gender, setGender] = useState("");
  const [contact, setContact] = useState("");
  const [uid, setUid] = useState("");
  const [user, setUser] = useState<UserDetails>();
  const [imagePath, setImagePath] = useState<String>();
  const [address, setAddress] = useState("");
  const [addressNo, setAddressNo] = useState("");
  const [city, setCity] = useState("");
  const [locationId, setLocationId] = useState("");
  const [description, setDesc] = useState("");

  const [userInfo, setuserInfo] = React.useState({
    file: new Blob(),
    filepreview: "",
    lol: false,
  });

  let navigate = useNavigate();

  const handleInputChange = (event: any) => {
    setuserInfo({
      ...userInfo,
      file: event.target.files[0],
      filepreview: URL.createObjectURL(event.target.files[0]),
      lol: true,
    });
  };

  React.useEffect(() => {
    if (uid != "") {
      updateProfile();
    }
    onAuthStateChanged(auth, (user) => {
      if (user) {
        setUid(user.uid);
      } else {
        // User not logged in or has just logged out.
      }
    });
  }, [uid]);

  React.useEffect(() => {
    if (locationId != "") {
      axios
        .patch(`/users/${user?.user.user_id}`, {
          name: name,
          surname: surname,
          gender: gender,
          contact: contact,
          tk_id_location: locationId,
          description: description,
        })
        .then(() => {
          const formdata = new FormData();
          if (userInfo.lol === true) {
            formdata.append("avatar", userInfo.file);
            axios.patch(`image/${user?.user.tk_id_image}`, formdata, {
                headers: { "Content-Type": "multipart/form-data" },
              })
                navigate("/user");
          } else {
            navigate("/user/profile/");
          }
        });
    }
  }, [locationId]);

  function nadjiSliku(idPic: number) {
    axios.get(`image/${idPic}`).then((response) => {
      setImagePath(response.data.path);
    });
  }

  function updateProfile() {
    axios.get(`/users/${auth.currentUser?.uid}`).then((response) => {
      setUser(response.data);
      nadjiSliku(response.data.user.tk_id_image);
    });
  }

  const saveProfile = async () => {
    if (city != "" || address != "" || addressNo != "") {
      axios
        .post("/location", {
          street: address || user?.location.street,
          street_no: addressNo || user?.location.street_no,
          city: city || user?.location.city,
        })
        .then((res) => {
          if (res.status == 200) {
            setLocationId(res.data.id);
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      setLocationId(user?.user.tk_id_location.toString() || "");
    }
  };

  return (
    <>
      <div className="container mx-auto my-5 bordered lg:flex no-wrap lg:-mx-2">
        <div className="w-full lg:w-3/12 lg:mx-2">
          <div className="bg-white p-3 border-t-4 border-[#6494ed]">
            {imagePath ? (
              <div className="image overflow-hidden">
                <img
                  className="h-auto w-full mx-auto"
                  src={"http://localhost:3030/" + imagePath}
                  alt="Profile Picture"
                />
              </div>
            ) : (
              <div className="image overflow-hidden">
                <img
                  className="h-auto w-full mx-auto"
                  src="http://localhost:3030/blank-profile-picture-973460_1280.png"
                  alt="Profile Picture"
                />
              </div>
            )}
            <h1 className="text-gray-900 leading-8 mt-3 mb-1 pb-2 text-[#6494ed]">
              {user?.user.name} {user?.user.surname}
            </h1>
            <textarea
              placeholder={
                user?.user.description ||
                `We dont know much about ${user?.user.name} ${user?.user.surname}, but we are sure they're great!`
              }
              className="h-[120px] w-full border p-2 resize-none"
              onChange={(event) => {
                setDesc(event.target.value);
              }}
            />
            <Button
              variant="outline-purple mt-3"
              className="w-full p-2"
              onClick={saveProfile}
            >
              Save
            </Button>
          </div>
        </div>

        <div className="w-full lg:w-9/12 lg:mx-2 flex flex-col lg:justify-center">
          <Form className="bg-white bordered">
            <div className="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
              <UserIcon className="h-5 text-[#6494ed]" />
              <span className="tracking-wide">Update your profile</span>
            </div>
            <div className="text-gray-700">
              <div className="grid grid-cols-1 lg:grid-cols-2 text-sm gap-2 text-gray-700">
                <div className="grid grid-cols-2">
                  <div className="px-4 py-2 font-semibold">First Name</div>
                  <Form.Control
                    type="text"
                    placeholder={user?.user.name}
                    onChange={(event) => {
                      setName(event.target.value);
                    }}
                  ></Form.Control>
                </div>
                <div className="grid grid-cols-2">
                  <div className="px-4 py-2 font-semibold">Last Name</div>
                  <Form.Control
                    type="text"
                    placeholder={user?.user.surname}
                    onChange={(event) => {
                      setSurname(event.target.value);
                    }}
                  ></Form.Control>
                </div>
                <div className="grid grid-cols-2">
                  <div className="px-4 py-2 font-semibold">Gender</div>
                  <Form.Control
                    type="text"
                    placeholder={user?.user.gender}
                    onChange={(event) => setGender(event.target.value)}
                  ></Form.Control>
                </div>
                <div className="grid grid-cols-2">
                  <div className="px-4 py-2 font-semibold">Contact No: </div>
                  <Form.Control
                    type="text"
                    placeholder={user?.user.contact}
                    onChange={(event) => setContact(event.target.value)}
                  ></Form.Control>
                </div>
                <div className="grid grid-cols-2">
                  <div className="px-4 py-2 font-semibold">City</div>
                  <Form.Control
                    type="text"
                    placeholder={user?.location.city}
                    onChange={(event) => setCity(event.target.value)}
                  ></Form.Control>
                </div>
                <div className="grid grid-cols-2">
                  <div className="px-4 py-2 font-semibold">Address</div>
                  <Form.Control
                    type="text"
                    placeholder={user?.location.street}
                    onChange={(event) => setAddress(event.target.value)}
                  ></Form.Control>
                </div>
                <div className="grid grid-cols-2">
                  <div className="px-4 py-2 font-semibold">Address No</div>
                  <Form.Control
                    type="text"
                    placeholder={user?.location.street_no}
                    onChange={(event) => setAddressNo(event.target.value)}
                  ></Form.Control>
                </div>
                <div className="grid grid-cols-2">
                  <div className="px-4 py-2 font-semibold">Email</div>
                  <div className="py-2">
                    <a
                      className="text-[#6494ed] no-underline"
                      href={"mailto:" + user?.user.email}
                    >
                      {user?.user.email}
                    </a>
                  </div>
                </div>
                <div className="lg:col-span-2 ml-4">
                  <label
                    className="px-2 py-2 font-semibold"
                    htmlFor="user_avatar"
                  >
                    Change your profile picture
                  </label>
                  <Form.Control
                    size="sm"
                    className="w-full mx-2"
                    id="user_avatar"
                    type="file"
                    onChange={handleInputChange}
                  />
                </div>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </>
  );
}
